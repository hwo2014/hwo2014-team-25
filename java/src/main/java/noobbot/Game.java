package noobbot;

import java.util.ArrayList;

public class Game {
	// git commit -am "germany!" && git push origin master
	// ./build && ./run testserver.helloworldopen.com 8091
	
	//./run hakkinen.helloworldopen.com 8091 sleep 2 keimola -p
	
	// keimola
	//private int gFactor = 1600;// working fine: 1500, 1400, 800; not: 1550
	//private double brakeFactor = 1000;//980,970, 370;// 4 (maybe grenzwertig) [automatisch geht auf 394.3675159532035]
					// auf 554.5465708220895
	// 1500 - 3 -> 07.55
	// 1500 - 3 -> 07.97
	// 1400 - 4 -> 8.15
	/*
	 * {length : ... } => straight piece
	 * {length AND switch:true}
	 *  length AND bridge: -> ignore
	 * {radius: 200 AND angle: 22.5} (angle negative for left turns)
	 */
	
	/*
	 * detect gFactor!!!!! -> works now
	 * TODO break factor with car in front is not working!
	 * TODO implement more than 2 lanes! and overtaking
	 * [ slow car in front inside curve -> flew out of track? ]
	 * turbo -> more intellligent start position
	 * TODO lap count on qualification? remaining laps < 0
	 * TODO no switching in CI
	 * TODO store data for track -> load it if that track reappears
	 */
	
	// germany:
	private int gFactor = 200;// 1600, 1700 works also
	private double brakeFactor = 200.0; // 100, 500, 550 geht [automatisch geht auf 434.9427061756796], 800 geht nicht
										//     geht jetzt auf 475.3687216638685
									// 1000 ist zu schnell
	private double acceleration = 10;
	
	
	private boolean started;
	private CarId myCar;
	private Race race;
	private CarPositionsMsg positions;
	private double[] entrySpeedsBase;
	private double[] entrySpeeds;
	private int lane = -1; // current lane
	private int laneStart = 0;
	private int lanePrevious = 0;
	private ArrayList<Integer> lanesOrdered;
	private int positionPieceIndex = 0;
	private int remainingLaps;
	private double positionPieceDistance = 0.0;
	private boolean switchScheduled = false;
	private int switchIndex;
	private final static boolean disableSwitching = false;
	private double lastThrottle = 0.0;
	private boolean firstRound = true;
	private int braking = 0;
	private double brakeDistance = 0.0;
	private double lastBrakingV = 0.0;
	private int accelerating = 0;
	private double accDistance = 0.0;
	private double lastAccV = 0.0;
	private double lastV = 0.0;
	private double lastVsoll = 0.0;
	private double lastAngle = 0.0;
	private double maxAngle = 54; // 60  * 0.9
	private double angleMax = 0.0;
	private double currentPieceAngle = 0.0;
	private static final int tickFactor = 60;
	private Turbo turbo = null;
	private int turboActive = 0;
	private double turboFactor = 1;
	private int realTurboActive = 0;
	private ArrayList<SpeedData> speedDataForCurve = null;

	public Game() {
	}
	
	public void started() {
		started = true;
	}
	
	public void finished() {
		started = false;
		initRace(race);
	}
	
	public boolean isRunning() {
		return started;
	}

	public double step(CarPositionsMsg positions) throws SwitchLaneException, StartTurboException {
		//if(!started) return 1;
		this.positions = positions;
		double throttleRound = 1;

		CarPosition myPosition = null;
		for(CarPosition position : positions.data) {
			if(position.id.name.equals(myCar.name) && position.id.color.equals(myCar.color)) {
				myPosition = position;
				break;
			}
		}
		if(myPosition == null) {
			System.out.println("Cannot find my car in position list");
		}
		int remainingLapsTmp = race.raceSession.laps;
		if(myPosition.piecePosition.lap > 0) {
			remainingLapsTmp -= myPosition.piecePosition.lap;
		}
		if(remainingLaps > remainingLapsTmp) {
			remainingLaps = remainingLapsTmp;
			System.out.println("remaining laps: " + remainingLapsTmp);
		}
		/*if(myPosition.piecePosition.lane.endLaneIndex != myPosition.piecePosition.lane.startLaneIndex) {
			System.out.println("LANE changed");
		}*/
		if(lane == -1) {
			lane = myPosition.piecePosition.lane.endLaneIndex;
			lanePrevious = lane;
			laneStart = lane;
			calculateEntrySpeeds();
		}
		if(laneStart != myPosition.piecePosition.lane.startLaneIndex && lane != myPosition.piecePosition.lane.endLaneIndex) {
			lanePrevious = laneStart;
			laneStart = myPosition.piecePosition.lane.startLaneIndex;
			lane = myPosition.piecePosition.lane.endLaneIndex;
			calculateEntrySpeeds();
		} else if((positionPieceIndex + 1)%race.track.pieces.length == myPosition.piecePosition.pieceIndex) {
			lanePrevious = laneStart;
		}
		
		// calculate difference in position
		double distance;
		if(positionPieceIndex == myPosition.piecePosition.pieceIndex) {
			distance = myPosition.piecePosition.inPieceDistance - positionPieceDistance;
			if(laneStart != lane) {
				distance = getSwitchLaneLength(race.track.pieces[positionPieceIndex], laneStart, lane, distance, true);
			}
		} else if((positionPieceIndex + 1)%race.track.pieces.length == myPosition.piecePosition.pieceIndex) {
			if(speedDataForCurve != null && (Math.abs(currentPieceAngle) > Math.abs(race.track.pieces[myPosition.piecePosition.pieceIndex].angle) ||
					((currentPieceAngle <= 0 && race.track.pieces[myPosition.piecePosition.pieceIndex].angle >=0) || (currentPieceAngle >= 0 && race.track.pieces[myPosition.piecePosition.pieceIndex].angle <= 0)))) {
				int slower = 0;
				double angleSum = 0.0;
				double thisMaxAngle = 0.0;
				for(SpeedData point : speedDataForCurve) {
					if(point.v < (point.v_soll - 10) || point.v > (point.v_soll + 10)) {
						slower ++;
					}
					angleSum += Math.abs(point.angle);
					if(thisMaxAngle < Math.abs(point.angle)) {
						thisMaxAngle = Math.abs(point.angle);
					}
				}
				if(slower / speedDataForCurve.size() > 0.6 || thisMaxAngle < angleMax || thisMaxAngle > maxAngle) {
					System.out.println("wrong speed (or angle) for evaluation");
				} else {
					angleMax = thisMaxAngle;
					//gFactor = (int) (gFactor * (1 + Math.pow((maxAngle - angleMax) / maxAngle, 3)));
					int increment = 200;
					if(angleMax < 1) {
						increment = 400;
					}
					gFactor = (int) (gFactor + increment * Math.pow((maxAngle - angleMax) / maxAngle, 1));
					System.out.println("angle average: " + angleSum / speedDataForCurve.size() + " and max " + angleMax + " => gFactor: " + gFactor);
					calculateEntrySpeeds();
				}
				speedDataForCurve = null;
			}
			if(speedDataForCurve == null && race.track.pieces[myPosition.piecePosition.pieceIndex].length == 0 && race.track.pieces[myPosition.piecePosition.pieceIndex].angle > 20) {
				// this piece is a curve
				speedDataForCurve = new ArrayList<>();
			}
			
			if(lanePrevious != laneStart) {//if(race.track.pieces[positionPieceIndex].isSwitch) {
				// lane change
				distance = getSwitchLaneLength(race.track.pieces[positionPieceIndex], 
						lanePrevious, laneStart, positionPieceDistance, false);
				distance += myPosition.piecePosition.inPieceDistance;
			} else {
				distance = getPieceLength(race.track.pieces[positionPieceIndex], myPosition.piecePosition.lane.startLaneIndex) - positionPieceDistance;
				distance += myPosition.piecePosition.inPieceDistance;
			}
		} else {
			distance = getPieceLength(race.track.pieces[positionPieceIndex], lane) - positionPieceDistance;
			for(int i = positionPieceIndex + 1; i != myPosition.piecePosition.pieceIndex; i ++) {
				if(i >= race.track.pieces.length) {
					i = 0;
					firstRound = false;
				}
				if(i == myPosition.piecePosition.pieceIndex) {
					break;
				}
				distance += getPieceLength(race.track.pieces[i], lane);
			}
			distance += myPosition.piecePosition.inPieceDistance;
		}
		positionPieceDistance = myPosition.piecePosition.inPieceDistance;
		boolean newPiece = false;
		if(positionPieceIndex !=myPosition.piecePosition.pieceIndex ){
			newPiece = true;
		}
		positionPieceIndex = myPosition.piecePosition.pieceIndex;
		lastAngle = myPosition.angle;
		currentPieceAngle = race.track.pieces[positionPieceIndex].angle;

		if(switchScheduled && positionPieceIndex == switchIndex) {
			switchScheduled = false;
		}
		
		double v = distance * tickFactor;
		
		int hasCheckedOtherCars = 0;
		boolean isSwitching = myPosition.piecePosition.lane.startLaneIndex != myPosition.piecePosition.lane.endLaneIndex;
		if(braking > 0) {
			brakeDistance += distance;
			hasCheckedOtherCars = 1;
			if(turboActive > 0 || hasCarInFront(isSwitching, false) || hasCarInBack(isSwitching, false)) {
				// can't touch other car while calculating
				if(turboActive == 0) {
					hasCheckedOtherCars = 2;
				}
				braking = 0;
				brakeDistance = 0;
			}
		}
		if(braking > 2) {
			double brakeFactorNew = (lastBrakingV * lastBrakingV - v * v) / (2 * brakeDistance);
			//double brakeFactorNew = (lastV - v) / ((braking - 2) * tickFactor);// v2 = v1 -a*t => a = (v1 - v2)  / t
			if(brakeFactorNew > brakeFactor) {
				brakeFactor = brakeFactorNew;
				//System.out.println("brake factor set to " + brakeFactorNew);
				calculateEntrySpeeds();
			}
		}
		if(accelerating > 0) {
			accDistance += distance;
			hasCheckedOtherCars = 1;
			if(/*turboActive > 0 || */hasCarInFront(isSwitching, false) || hasCarInBack(isSwitching, false)) {
				// can't touch other car while calculating
				if(turboActive == 0) {
					hasCheckedOtherCars = 2;
				}
				accelerating = 0;
				accDistance = 0;
			}
			if(accelerating > 2) {
				double newAcceleration = (v * v - lastAccV * lastAccV) / (2 * accDistance * turboFactor);
				if(newAcceleration > acceleration) {
					acceleration = newAcceleration;
					System.out.println("new acceleration: " + acceleration);
				}
			}
		}
		double v_soll;
		if(entrySpeeds[(myPosition.piecePosition.pieceIndex + 1) % entrySpeeds.length] >= entrySpeeds[myPosition.piecePosition.pieceIndex]) {
			double slider = Math.pow(positionPieceDistance / getPieceLength(race.track.pieces[positionPieceIndex], lane), 2) / turboFactor;
			v_soll = entrySpeeds[myPosition.piecePosition.pieceIndex] +
					(entrySpeeds[(myPosition.piecePosition.pieceIndex + 1) % entrySpeeds.length] - entrySpeeds[myPosition.piecePosition.pieceIndex]) * slider;
		} else if(remainingLaps == 1 && (myPosition.piecePosition.pieceIndex + 1) == entrySpeeds.length) {
			v_soll = entrySpeeds[myPosition.piecePosition.pieceIndex];
		} else {
			// distance to next track-piece
			double distanceNext = getPieceLength(race.track.pieces[positionPieceIndex], lane) - positionPieceDistance;
			v_soll = calculateStartVelocity(entrySpeeds[(myPosition.piecePosition.pieceIndex + 1) % entrySpeeds.length], distanceNext);
			/* ??? not faster? causes (currently) no crashs
			 */ if(v_soll > entrySpeeds[myPosition.piecePosition.pieceIndex]) {
				v_soll = entrySpeeds[myPosition.piecePosition.pieceIndex];
			}
		}
		if(braking > 2 && v > v_soll*1.1) {
			// TODO System.out.println("SHOULD REDUCE BRAKING FACTOR!!!!!!!!!!!!!!!!!!!!!!");
			if(brakeFactor <= 200.0) {
				brakeFactor = brakeFactor / 2;
				System.out.println("reduced brake factor");
				calculateEntrySpeeds();
			}
		}
		if(speedDataForCurve != null) {
			SpeedData newData = new SpeedData();
			newData.v = v;
			newData.v_soll = v_soll;
			newData.angle = myPosition.angle;
			speedDataForCurve.add(newData);
		}
		double requestedA = (v_soll * v_soll - v * v) / (2 * distance * turboFactor);
		if(myPosition.angle > maxAngle) {
			requestedA = 0;
		}
		if(requestedA > acceleration) {
			throttleRound = 1;
			accelerating ++;
			if(hasCheckedOtherCars == 2 || hasCheckedOtherCars == 0 && (hasCarInFront(isSwitching, false) || hasCarInBack(isSwitching, false))) {
				// can't touch other car while calculating
				accelerating = 0;
				accDistance = 0;
			}
			if(accelerating == 1) {
				lastAccV = v;
				accDistance = 0.0;
			}
			if(accelerating == 2) {
				lastAccV = v;
				accDistance = distance;
			}
			braking = 0;
			brakeDistance = .0;
		} else if(requestedA > turboFactor) {
			double divider = brakeFactor / (brakeFactor + acceleration);
			throttleRound = (1 - divider) * (requestedA / acceleration) + divider;
			accelerating = 0;
			accDistance = 0;
			braking = 0;
			brakeDistance = .0;
		} else/* if(requestedA < (-1) * brakeFactor)*/ {
			throttleRound = 0;
			braking ++;
			if(turboActive > 0 || hasCheckedOtherCars == 2 || hasCheckedOtherCars == 0 && (hasCarInFront(isSwitching, false) || hasCarInBack(isSwitching, false))) {
				// can't touch other car while calculating
				braking = 0;
				brakeDistance = 0;
			}
			if(braking == 1) {
				lastBrakingV = v;
				brakeDistance = 0.0;
			}
			if(braking == 2) {
				lastBrakingV = v;
				brakeDistance = distance;
			}
			accelerating = 0;
			accDistance = 0;
		}
		if(/*newPiece){/*/turboActive > 0 && v != 0.0 || positions.gameTick %20 == 0 && v != 0.0) {
			//System.out.println(""+distance+" Speed " + v+" \t"+ v_soll+" \t"+entrySpeeds[myPosition.piecePosition.pieceIndex]+" \t" + entrySpeeds[(myPosition.piecePosition.pieceIndex + 1) % entrySpeeds.length]+" \t[" + race.track.pieces[myPosition.piecePosition.pieceIndex].isSwitch);//entrySpeedsBase[(myPosition.piecePosition.pieceIndex + 1) % entrySpeeds.length] + "]");
			System.out.println(throttleRound + ", "+v +" < "+ v_soll +" "+race.track.pieces[myPosition.piecePosition.pieceIndex].isSwitch+ " "+(myPosition.piecePosition.lane.startLaneIndex != myPosition.piecePosition.lane.endLaneIndex) + " @ angle " + myPosition.angle);
		}
		// maybe send a lane switch
		if(lastThrottle == throttleRound && !disableSwitching && !switchScheduled) {
			int switching = calculateSwitching();
			switchScheduled = true;
			if(lastThrottle == 0 && switching != 0 && braking > 0) {
				braking ++;
			}
			if(lastThrottle == 1 && switching != 0 && accelerating > 0) {
				accelerating ++;
			}
			if(switching == 1) {
				throw new SwitchLaneException("right");
			} else if(switching == -1) {
				throw new SwitchLaneException("left");
			}
		}
		// start turbo
		if(turbo != null && turboActive == 0 && throttleRound == 1 && lastThrottle == throttleRound) {
			/*if(lastThrottle == throttleRound && lastThrottle == 1 && braking > 0) {
				braking ++;
			}*/
			turboActive = turbo.turboDurationTicks;
			realTurboActive = turboActive;
			turboFactor = turbo.turboFactor;
			System.out.println("Launch TURBO (for " + turboActive + " rounds)");
			throw new StartTurboException();
		}
		if(turbo != null && turboActive != 0) {
			turboActive --;
			realTurboActive --;
			if(turboActive == 0) {
				if(lastVsoll >= v_soll) {
					//turboActive ++;
					turboActive = 1;
				} else {
					turbo = null;
					System.out.println("Turbo end");
				}
			}
			if(realTurboActive == 0) {
				turboFactor = 1;
				accelerating = 0;
				accDistance = 0;
			}
		}
		lastV  = v;
		lastVsoll = v_soll;
		lastThrottle = throttleRound;
		return throttleRound;
	}

	public void setOwnCar(CarId data) {
		myCar = data;
	}

	public void initRace(Race race) {
		this.race = race;
		turbo = null;
		turboActive = 0;
		turboFactor = 1;
		realTurboActive = 0;
		lane = -1;
		laneStart = 0;
		lanePrevious = 0;
		firstRound = true;
		positionPieceDistance = 0.0;
		positionPieceIndex = 0;
		lastThrottle = 0.0;
		switchIndex = -1;
		switchScheduled = false;
		entrySpeedsBase = new double[race.track.pieces.length];
		remainingLaps = race.raceSession.laps;
		if(remainingLaps < 1) {
			remainingLaps = Integer.MAX_VALUE;
			System.out.println("Qualifying -> inifinite laps");
		}
		lastV = 0.0;
		lastVsoll = 0.0;
		braking = 0;
		brakeDistance = 0.0;
		accelerating = 0;
		accDistance = 0.0;
		speedDataForCurve = null;
		currentPieceAngle = 0.0;
		
		// order lanes ascending
		lanesOrdered = new ArrayList<>();
		for(TrackLane lane : race.track.lanes) {
			boolean added = false;
			for(int i = 0; i < lanesOrdered.size(); i ++) {
				if(race.track.lanes[lanesOrdered.get(i)].distanceFromCenter > lane.distanceFromCenter) {
					added = true;
					lanesOrdered.add(i, lane.index);
					break;
				}
			}
			if(!added) {
				lanesOrdered.add(lane.index);
			}
		}
	}
	
	
	private void calculateEntrySpeeds() {
		System.out.println("g factor set to " + gFactor + ", break factor set to " + brakeFactor);
		for(int i = 0; i < entrySpeedsBase.length; i ++) {
			TrackPiece piece = race.track.pieces[i];
			if(piece.length != 0) {
				entrySpeedsBase[i] = Double.MAX_VALUE;
				continue;
			}
			entrySpeedsBase[i] = Math.sqrt(newRadius(piece, lane) * gFactor);
		}
		if(remainingLaps == 1) {
			// last lap: end is unlimited fast
			entrySpeedsBase[entrySpeedsBase.length - 1] = Double.MAX_VALUE;
		}
		calculateSpeedPath();
	}
	
	
	private int newRadius(TrackPiece piece, int laneNumber) {
		// get lane distance to center
		int diff = race.track.lanes[laneNumber].distanceFromCenter;
		int newRadius = piece.radius;
		if(piece.angle < 0) {
			// left turn
			newRadius += diff;
		} else {
			// right turn
			newRadius -= diff;
		}
		return newRadius;
	}
	
	private void calculateSpeedPath() {
		entrySpeeds = new double[entrySpeedsBase.length];
		entrySpeeds[entrySpeedsBase.length - 1] = entrySpeedsBase[entrySpeedsBase.length - 1];
		
		int startCalculationAt = /*entrySpeedsBase.length */- 1;
		if(remainingLaps != 1) {
			// get slowest point
			double slowestSpeed = Double.MAX_VALUE;
			for(int i = 0; i < entrySpeedsBase.length; i ++) {
				if(entrySpeedsBase[i] < slowestSpeed) {
					slowestSpeed = entrySpeedsBase[i];
					startCalculationAt = i;
				}
			}
			entrySpeeds[startCalculationAt] = slowestSpeed;
		}
		for(int i = startCalculationAt + entrySpeedsBase.length; i > startCalculationAt + 1; i --) {
			double tmpV = calculateStartVelocity(entrySpeeds[i%entrySpeedsBase.length], 
					getPieceLength(race.track.pieces[(i - 1)%entrySpeedsBase.length], lane));
			if(tmpV < entrySpeedsBase[(i - 1)%entrySpeedsBase.length]) {
				entrySpeeds[(i - 1)%entrySpeedsBase.length] = tmpV;
			} else {
				entrySpeeds[(i - 1)%entrySpeedsBase.length] = entrySpeedsBase[(i - 1)%entrySpeedsBase.length]; 
			}
		}
	}
	
	private double getSwitchLaneLength(TrackPiece piece, int startLane, int endLane, double traveledDistance, boolean first) {
		if(first) {
			// for a distance traveled on that piece
			if(piece.length != 0) {
				// gerader switch
				int laneDiff = Math.abs(race.track.lanes[startLane].distanceFromCenter - race.track.lanes[endLane].distanceFromCenter);
				double pieceLength = getPieceLength(piece, startLane);
				double travelDiff = traveledDistance * laneDiff / pieceLength;
				return Math.sqrt(traveledDistance * traveledDistance + travelDiff * travelDiff);
			} else {
				// TODO
				System.out.println("switch curve II TODO");
				// gebogener switch
				/*double startLaneLength = getPieceLength(race.track.pieces[positionPieceIndex], startLane);
				double endLaneLength = getPieceLength(race.track.pieces[positionPieceIndex], endLane);
				double diff = Math.abs(startLaneLength - endLaneLength);*/
				return traveledDistance;
			}
		} else {
			// for a distance traveled after positionPieceDistance to the end of the piece
			if(piece.length != 0) {
				// gerader switch
				int laneDiff = Math.abs(race.track.lanes[startLane].distanceFromCenter - race.track.lanes[endLane].distanceFromCenter);
				double pieceLength = getPieceLength(piece, startLane);
				double result = Math.sqrt(pieceLength * pieceLength + laneDiff * laneDiff) - traveledDistance;
				//traveledDistance = pieceLength - traveledDistance;
				//double travelDiff = traveledDistance * laneDiff / pieceLength;
				//double result = Math.sqrt(traveledDistance * traveledDistance + travelDiff * travelDiff);
				//System.out.println("switch straight: " + result);
				return result;
			} else {
				// gebogener switch, works pretty well
				/*double startLaneLength = getPieceLength(race.track.pieces[positionPieceIndex], startLane);
				double endLaneLength = getPieceLength(race.track.pieces[positionPieceIndex], endLane);
				double diff = Math.abs(startLaneLength - endLaneLength);*/
				System.out.println("switch curve");
				double travel1 = getPieceLength(piece, startLane);
				double travel2 = getPieceLength(piece, endLane);
				double travelDiff = Math.abs(travel1 - travel2);
				return getPieceLength(piece, endLane) - traveledDistance;
			}
		}
	}
	
	private double calculateStartVelocity(double endVelocity, double length) {
		return Math.sqrt(Math.pow(endVelocity, 2) + 2 * brakeFactor * length);
	}

	private double getPieceLength(TrackPiece piece, int laneNumber) {
		if(piece.length != 0) {
			return piece.length;
		} else {
			return Math.PI * Math.abs(piece.angle) * newRadius(piece, laneNumber) / 180;
		}
	}
	
	
	private double getLengthBetween(int start, int end, int laneNumber) {
		double distance = 0;

		for(int i = start; i != end + 1; i ++) {
			if(i >= race.track.pieces.length) {
				i = 0;
			}
			distance += getPieceLength(race.track.pieces[i], laneNumber);
			if(end == i + 1) {
				break;
			}
		}
		return distance;
	}
	
	private int calculateSwitching() {
		int nextSwitch = -1;
		int nextNextSwitch = -1;
		for(int i = positionPieceIndex + 1; i != positionPieceIndex; i ++) {
			if(i >= race.track.pieces.length) {
				i = 0;
			}
			if(race.track.pieces[i].isSwitch) {
				nextSwitch = i;
				break;
			}
			if(positionPieceIndex == i) {
				break;
			}
		}
		if(nextSwitch == -1) {
			// no switch available
			return 0;
		} else if(nextSwitch <= positionPieceIndex && remainingLaps < 2) {
			// next switch is in next lap
			return 0;
		}

		for(int i = nextSwitch + 1; i != nextSwitch; i ++) {
			if(i >= race.track.pieces.length) {
				i = 0;
			}
			if(race.track.pieces[i].isSwitch) {
				nextNextSwitch = i;
				break;
			}
			if(nextSwitch == i) {
				break;
			}
		}
		if(nextNextSwitch == -1) {
			// no switch available -> should not happen
			return 0;
		} else if(nextSwitch <= positionPieceIndex && nextNextSwitch <= nextSwitch && remainingLaps < 3) {
			// next switch is in next lap AND nextNext switch is in over next lap
			nextNextSwitch = 0; // don't care
		} else if(nextNextSwitch <= nextSwitch && remainingLaps < 2) {
			// nextNext switch is in next lap
			nextNextSwitch = 0; // don't care
		}
		// don't include (start and end) switch in calculation
		nextSwitch = (nextSwitch + 1) % race.track.pieces.length;
		switchIndex = nextSwitch;
		nextNextSwitch = nextNextSwitch - 1;
		if(nextNextSwitch < 0) {
			nextNextSwitch = race.track.pieces.length + nextNextSwitch;
		}
		
		// calculate distance in current lane
		int currentLane = lanesOrdered.indexOf(lane);
		double currentDistance = getLengthBetween(nextSwitch, nextNextSwitch, lane);
		
		// in right lane
		double rightDistance = Double.MAX_VALUE;
		if(currentLane < lanesOrdered.size() - 1) {
			rightDistance = getLengthBetween(nextSwitch, nextNextSwitch, lanesOrdered.get(currentLane + 1));
		}
		
		// in left lane
		double leftDistance = Double.MAX_VALUE;
		if(currentLane > 0) {
			leftDistance = getLengthBetween(nextSwitch, nextNextSwitch, lanesOrdered.get(currentLane - 1));
		}
		//System.out.println(lane + "/"+currentLane +" from" + lanesOrdered.size()+" switch " + positionPieceIndex + " - "+ nextSwitch + " - " + nextNextSwitch +" - left " + leftDistance + ", current " + currentDistance + ", right " + rightDistance);
		if(leftDistance < currentDistance && leftDistance < rightDistance) {
			// left is shortest
			return -1;
		}
		if(rightDistance < currentDistance && rightDistance < leftDistance) {
			return 1;
		}
		// otherwise: stay in lane
		return 0;
	}
	
	private boolean hasCarInFront(boolean switching, boolean printOut) {
		// assumes the newest piece data is already stored
		//if(switching) {
		/*if(switching) {
		 	// kicks out of track!!!
			return false;
		}*/
			/// TODO
		//} else {
		// TODO case: i'm switching, opponent is in front (not switching)?
		double myLength = 0.0;
		double front = 0.0;
		double back = 0.0;
		for(CarsInit car : race.cars) {
			if(car.id.name.equals(myCar.name) && car.id.color.equals(myCar.color)) {
				myLength = car.dimensions.length;
				front = car.dimensions.guideFlagPosition;
				back = myLength - front;
			}
		}
		for(CarPosition position : positions.data) {
			if(position.id.name.equals(myCar.name) && position.id.color.equals(myCar.color)) {
			} else {
				CarsInit thisCar = null;
				for(CarsInit car : race.cars) {
					if(car.id.name.equals(position.id.name) && car.id.color.equals(position.id.color)) {
						thisCar = car;
					}
				}
				if(thisCar == null) {
					continue;
				}
				if(position.piecePosition.lane.startLaneIndex != lane && position.piecePosition.lane.endLaneIndex != lane
						&& position.piecePosition.lane.startLaneIndex != laneStart  && position.piecePosition.lane.endLaneIndex != laneStart) {
					continue;
				}
				double pieceLengthSum = 0;
				if(positionPieceIndex != position.piecePosition.pieceIndex) {
					if(positionPieceIndex > position.piecePosition.pieceIndex) {
						continue;
					}
					pieceLengthSum = getLengthBetween(positionPieceIndex, (position.piecePosition.pieceIndex + 1)
							% race.track.pieces.length, lane);
				} else if(position.piecePosition.inPieceDistance < positionPieceDistance) {
					continue;
				}
				double diff = pieceLengthSum
						+ position.piecePosition.inPieceDistance - thisCar.dimensions.length 
						+ thisCar.dimensions.guideFlagPosition 
						- positionPieceDistance - front;
				/*System.out.println(diff +": ");/* + pieceLengthSum  +"- "+position.piecePosition.inPieceDistance+
						"- "+thisCar.dimensions.length+"- "+thisCar.dimensions.guideFlagPosition+"- "+positionPieceDistance
						+"- "+front);*/
				if(diff < 50 && diff >= 0) {
					if(printOut) {
						System.out.println("DIFF to front man: " + diff + "( <50.0 )");
					}
					return true;
				}
			}
		}
		//}
		return false;
	}
	
	private boolean hasCarInBack(boolean switching, boolean printOut) {
		// copied from hasCarInFront
		double myLength = 0.0;
		double front = 0.0;
		double back = 0.0;
		for(CarsInit car : race.cars) {
			if(car.id.name.equals(myCar.name) && car.id.color.equals(myCar.color)) {
				myLength = car.dimensions.length;
				front = car.dimensions.guideFlagPosition;
				back = myLength - front;
			}
		}
		for(CarPosition position : positions.data) {
			if(position.id.name.equals(myCar.name) && position.id.color.equals(myCar.color)) {
			} else {
				CarsInit thisCar = null;
				for(CarsInit car : race.cars) {
					if(car.id.name.equals(position.id.name) && car.id.color.equals(position.id.color)) {
						thisCar = car;
					}
				}
				if(thisCar == null) {
					continue;
				}
				if(position.piecePosition.lane.startLaneIndex != lane && position.piecePosition.lane.endLaneIndex != lane
						&& position.piecePosition.lane.startLaneIndex != laneStart  && position.piecePosition.lane.endLaneIndex != laneStart) {
					continue;
				}
				double pieceLengthSum = 0;
				if(positionPieceIndex != position.piecePosition.pieceIndex) {
					if(positionPieceIndex < position.piecePosition.pieceIndex) {
						continue;
					}
					pieceLengthSum = getLengthBetween(position.piecePosition.pieceIndex, (positionPieceIndex + 1)
							% race.track.pieces.length, lane);
				} else if(position.piecePosition.inPieceDistance > positionPieceDistance) {
					continue;
				}
				double diff = pieceLengthSum
						- position.piecePosition.inPieceDistance// - thisCar.dimensions.length 
						- thisCar.dimensions.guideFlagPosition 
						+ positionPieceDistance - back;
				if(diff < 50 && diff >= 0) {
					if(printOut) {
						System.out.println("DIFF to BACK man: " + diff + "( <50.0 )");
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public void flewOutOfTrack(CarId data) {
		if(data != null) {
			if(data.name.equals(myCar.name) && data.color.equals(myCar.color)) {
				System.out.println("CRASH: flew out of track :(" + lastV + ", " + lastVsoll + " with angle " + lastAngle);
				maxAngle = lastAngle * 0.9;
				angleMax = 0.0;
				if(lastV != 0.0 && lastVsoll != 0.0 && (lastV > (lastVsoll + 20))) {
					System.out.println("recalculating braking");
					brakeFactor = brakeFactor / 2;
					braking = 0;
					brakeDistance = 0;
					calculateEntrySpeeds();
					return;
					// don't halve the gFactor -> maybe breakFactor is enougth to modify
				}
			} else {
				System.out.println("opponent flew out of track *MUHAHA*");
				return;
			}
		} else {
			System.out.println("Flew out of track, but no data was provided");
		}
		braking = 0;
		brakeDistance = 0;
		// halve the g_factor
		gFactor = gFactor / 2;
		calculateEntrySpeeds();
	}

	public void setTurbo(Turbo data) {
		System.out.println("Got TURBO");
		if(data.turboDurationTicks <= 0) {
			System.out.println("but only for <= 0 ticks...");
			return;
		}
		this.turbo = data;
	}
}

class SpeedData {
	public Double v;
	public Double v_soll;
	public Double angle;
}
