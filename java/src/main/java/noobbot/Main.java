package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {
	private Game game = new Game();
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        int carCount = -1;
        String track = "";
        String password = "";
        if(args.length == 7) {
	        carCount = Integer.parseInt(args[4]);
	        track = args[5];
	        password = args[6];
        	System.out.println("starting in enhanced mode");
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey), carCount, track, password);
        socket.close();
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join, final int carCount, String track, String password) throws IOException {
        this.writer = writer;
        String line = null;
        if(carCount == -1) {
        	send(join);
        } else {
	        if(track.equals("-t")) {
	        	track = "";
	        }
	        if(password.equals("-p")) {
	        	password = null;
	        }
	        if(track.equals("") && carCount == 1) {
	        	send(join);
	        	System.out.println("starting default race");
	        } else {
	            send(new JoinRace(join.name, join.key, track, password, carCount));
	            System.out.println("Starting game: track " + track + ", password " + password + 
	            		" and "+ carCount + " cars");
	        }
        }

        //send(join);
        //send(new CreateRace(join.name, join.key, "keimola", "", 1));
        //send(new CreateRace(join.name, join.key, "germany", "", 1));
        //send(new JoinRace(join.name, join.key, "keimola", "", 1));
        //send(new JoinRace(join.name, join.key, "germany", "", 1));
        //send(new JoinRace(join.name, join.key, "germany", "", 2));
        
        while((line = reader.readLine()) != null) {
        	//System.out.println(line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                //System.out.println("carPositions received");
            	/* data only results in:
            	 * [{"id":{"name":"JustDoIt","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0.0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0.0,"endLaneIndex":0.0},"lap":0.0}}]
            	 */
            	final CarPositionsMsg positions = gson.fromJson(line, CarPositionsMsg.class);
                try {
                	/*if(join.name.equals("sleep")) {
                		break;
                	}*/
					send(new Throttle(game.step(positions)));
				} catch (SwitchLaneException e) {
					//System.out.println(" ========================= switching lane to the " + e.getMessage());
					if(e.getMessage().equals("left")) {
						// change lane to the left
						send(new SwitchLaneLeft());
					} else {
						// change lane to the right
						send(new SwitchLaneRight());
					}
				} catch(StartTurboException e) {
					send(new StartTurbo());
				}
            // crash
            } else {
            	if (msgFromServer.msgType.equals("crash")) {
	            	game.flewOutOfTrack(gson.fromJson(line, Crash.class).data);
	            // TODO spawn
	            // TODO lapFinished
	            //
	            } else if (msgFromServer.msgType.equals("join")) {
	                System.out.println("Joined");
	            } else if (msgFromServer.msgType.equals("yourCar")) {
	            	game.setOwnCar(gson.fromJson(line, MyCar.class).data);
	                System.out.println("MyCar received");
	            } else if (msgFromServer.msgType.equals("gameInit")) {
	            	final Race race = gson.fromJson(line, GameInit.class).data.race;
	            	game.initRace(race);
	                System.out.println("Race init");
	            } else if (msgFromServer.msgType.equals("gameEnd")) {
	                System.out.println("Race end");
	                game.finished();
	            } else if (msgFromServer.msgType.equals("gameStart")) {
	                System.out.println("Race start");
	                game.started();
	                // if contains gameTick ->
	                send(new Ping());
	            } else if (msgFromServer.msgType.equals("turboAvailable")) {
	            	game.setTurbo(gson.fromJson(line, TurboMsg.class).data);
	            // TODO tournamentEnd
	            }
                send(new Ping());
            }
        }
        System.out.println("Network finished");
        writer.close();
    }

	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class MyCar {
	public String msgType;
	public CarId data;
}

class Crash {
	public String msgType;
	public CarId data;
}

class GameInit {
	public String msgType;
	public ReisWrapper data;
}

class ReisWrapper {
	public Race race;
}

class Race {
	public Track track;
	public CarsInit[] cars;
	public Session raceSession;
}

class Track {
	public String id;
	public String name;
	public TrackPiece[] pieces;
	public TrackLane[] lanes;
	public Object startingPoint; // don't care
}

class TrackPiece {
	public double length;
	
	@SerializedName("switch")
	public boolean isSwitch;
	public int radius;
	public double angle;
	// bridge
}

class TrackLane {
	public int distanceFromCenter;
	public int index;
}

class CarsInit {
	public CarId id;
	public CarDimensions dimensions;
}

class CarDimensions {
	public double length;
	public double width;
	public double guideFlagPosition;
}

class Session {
	public int laps = Integer.MAX_VALUE;
	public int maxLapTimeMs;
	boolean quickRace;
}

class CarPositionsMsg {
    public final String msgType;
    public final CarPosition[] data;
    public final String gameId;
    public final int gameTick;

	CarPositionsMsg(final String msgType, final CarPosition[] data, final String gameId, final int gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

class CarPosition {
	public CarId id;
	public double angle;
	public PiecePosition piecePosition;
}

class PiecePosition {
	public int pieceIndex;
	public double inPieceDistance;
	public Lane lane;
	public int lap;
}

class Lane {
	public int startLaneIndex;
	public int endLaneIndex;
}

class TurboMsg {
	public String msgType;
	public Turbo data;
}

class Turbo {
	//public double turboDurationMilliseconds;
	public int turboDurationTicks;
	public double turboFactor;
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class CreateRace extends SendMsg {
	public BotId botId = new BotId();
    public final String trackName;
    public final String password;
    public final int carCount;
    

    CreateRace(final String name, final String key, final String trackName, final String password, final int carCount) {
    	botId.name = name;
    	botId.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class JoinRace extends SendMsg {
	public BotId botId = new BotId();
    public final String trackName;
    public final String password;
    public final int carCount;
    

    JoinRace(final String name, final String key, final String trackName, final String password, final int carCount) {
    	botId.name = name;
    	botId.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class BotId {
    public String name;
    public String key;
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLaneLeft extends SendMsg {
	private String value = "Left";

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class SwitchLaneRight extends SendMsg {
	private String value = "Right";

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class StartTurbo extends SendMsg {
	//{"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
	private String value = "Beam me to the finish!";

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}