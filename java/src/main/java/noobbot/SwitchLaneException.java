package noobbot;

public class SwitchLaneException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SwitchLaneException(String direction) {
		super(direction);
	}

}
